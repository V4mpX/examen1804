const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')

router.get('/login', (req, res) => {
  userController.login(req, res)
})

module.exports = router
