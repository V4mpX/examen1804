const express = require('express')
const router = express.Router()
const materiaController = require('../controllers/materiaController.js')
const auth = require('../middlewares/auth.js')

router.get('/', (req, res) => {
  materiaController.index(req, res)
})

router.get('/privada', auth, (req, res) => {
  materiaController.privada(req, res)
})

router.get('/:id', (req, res) => {
  materiaController.show(req, res)
})

router.post('/', (req, res) => {
  materiaController.create(req, res)
})

router.put('/:id', (req, res) => {
  materiaController.update(req, res)
})

router.delete('/:id', (req, res) => {
  materiaController.remove(req, res)
})

module.exports = router
