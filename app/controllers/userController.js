const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

const login = (req, res) => {
  const user = new User({
    codigo: req.body.codigo,
    nombre: req.body.nombre,
    password: req.body.password
  })

  if (user.nombre != 'pepe' && user.nombre != 'juan') {
    res.status(500).send({ message: `Error al logear usuario: nombre` })
  }
  if (user.password != 'secret') {
    res.status(500).send({ message: `Error al logear usuario: password` })
  }
  return res.status(200).send({ token: servicejwt.createToken(user) })
}
module.exports = {
  login
}
