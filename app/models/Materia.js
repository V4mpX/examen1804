const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  codigo: {
    type: String,
    required: true,
    unique: true
  },
  nombre: String,
  curso: String,
  horas: Number
})

const Materia = mongoose.model('materia', materiaSchema)

module.exports = Materia
