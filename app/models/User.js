const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  codigo: {
    type: String,
    required: true,
    unique: true
  },
  nombre: String,
  password: String
})

const User = mongoose.model('user', userSchema)

module.exports = User
